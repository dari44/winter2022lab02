import java.util.Scanner;

class PartThree{
	public static void main(String[] args){
		Scanner keyboard = new Scanner(System.in);
		double sideSquare = keyboard.nextDouble();
		double firstSideRect = keyboard.nextDouble();
		double secondSideRect = keyboard.nextDouble();
		
		AreaComputations ac = new AreaComputations();
		
		double squareArea = AreaComputations.areaSquare(sideSquare);
		double rectArea = ac.areaRectangle(firstSideRect, secondSideRect);
		
		System.out.println(squareArea + ", " + rectArea);
	}
}